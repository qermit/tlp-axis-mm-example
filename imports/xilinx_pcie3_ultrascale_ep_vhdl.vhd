----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/04/2020 11:12:53 PM
-- Design Name: 
-- Module Name: xilinx_pcie3_ultrascale_ep_vhdl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;
use work.pcie3_mm_components_pkg.all;

entity xilinx_pcie3_uscale_ep_vhdl is
    generic (
      C_DATA_WIDTH: positive:=256;
      EXT_PIPE_SIM: string:= "FALSE";
      PL_LINK_CAP_MAX_LINK_SPEED: natural:= 4; -- 1- GEN1, 2 - GEN2, 4 - GEN3
      PL_LINK_CAP_MAX_LINK_WIDTH: natural:= 8  -- 1- X1, 2 - X2, 4 - X4, 8 - X8      
    );
    Port ( 
      pci_exp_txp : out std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
      pci_exp_txn : out std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
      pci_exp_rxp : in std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
      pci_exp_rxn : in std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
      
      sys_clk_p: in std_logic;
      sys_clk_n: in std_logic;
      sys_rst_n: in std_logic
    );
end xilinx_pcie3_uscale_ep_vhdl;


architecture Behavioral of xilinx_pcie3_uscale_ep_vhdl is

signal sys_rst_n_c: std_logic;
signal led_out: std_logic_vector(7 downto 0);

component wb_dummy_slave is
    Port ( clk_i : in STD_LOGIC;
    rst_n_i : in std_logic;
               -- Wishbone master interface
           m_wb_m2s: in t_wishbone_a32_d32_m2s;
           m_wb_s2m: out t_wishbone_a32_d32_s2m
    );
end component wb_dummy_slave;

COMPONENT pcie3_tlp_interconnect is
  generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_s2m: out t_axis_cq_s2m;           
         m_axis_cc_m2s: out t_axis_cc_m2s;
         m_axis_cc_s2m: in  t_axis_cc_s2m;
           
         m_axis_cq_m2s: out a_axis_cq_m2s(2 downto 0);
         m_axis_cq_s2m: in  a_axis_cq_s2m(2 downto 0);
         s_axis_cc_m2s: in  a_axis_cc_m2s(2 downto 0);
         s_axis_cc_s2m: out a_axis_cc_s2m(2 downto 0)
);
end  COMPONENT pcie3_tlp_interconnect;

--constant c_tlp_routing: a_pcie_gen3_routing(2 downto 0) := (
--   0 => f_routing_pcie_gen3_record(p_pf_id => 0),
--   1 => f_routing_pcie_gen3_record(p_bar_id => 0),
--   others => c_pcie_gen3_routing_record_disabled
--);

constant c_tlp_routing: a_pcie_gen3_routing(2 downto 0) := (
   0 => c_pcie_gen3_routing_record_disabled,
   1 => f_routing_pcie_gen3_record(p_bar_id => 0),
   others => c_pcie_gen3_routing_record_disabled
);

signal         s_axis_cq_m2s: t_axis_cq_m2s := c_axis_cq_m2s_zero;
signal         s_axis_cq_s2m: t_axis_cq_s2m := (others => '0');
signal         m_axis_cc_m2s: t_axis_cc_m2s := c_axis_cc_m2s_zero;
signal         m_axis_cc_s2m: t_axis_cc_s2m := (others => '0');

signal         m_axis_cq_m2s: a_axis_cq_m2s(2 downto 0) := (others => c_axis_cq_m2s_zero);
signal         m_axis_cq_s2m: a_axis_cq_s2m(2 downto 0) := (others => (others => '0'));
signal         s_axis_cc_m2s: a_axis_cc_m2s(2 downto 0) := (others => c_axis_cc_m2s_zero);
signal         s_axis_cc_s2m: a_axis_cc_s2m(2 downto 0) := (others => (others => '0'));
signal         m_axis_cc_s2m_tready:std_logic_vector(3 downto 0) := (others => '0');         

signal m_axis_cq_s2m_0_tready: std_logic_vector(21 downto 0) := (others => '0');    
signal s_axis_cc_s2m_0_tready: std_logic_vector(3 downto 0) := (others => '0');     

--- tmp signals
--signal s_axis_cq_s2m_tready: std_logic_vector(21 downto 0);

signal user_reset_n: std_logic;
COMPONENT pcie3_ultrascale_0
  PORT (
    pci_exp_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;
    s_axis_rq_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_rq_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_rq_tlast : IN STD_LOGIC;
    s_axis_rq_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_rq_tuser : IN STD_LOGIC_VECTOR(59 DOWNTO 0);
    s_axis_rq_tvalid : IN STD_LOGIC;
    m_axis_rc_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_rc_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_rc_tlast : OUT STD_LOGIC;
    m_axis_rc_tready : IN STD_LOGIC;
    m_axis_rc_tuser : OUT STD_LOGIC_VECTOR(74 DOWNTO 0);
    m_axis_rc_tvalid : OUT STD_LOGIC;
    m_axis_cq_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_cq_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_cq_tlast : OUT STD_LOGIC;
    m_axis_cq_tready : IN STD_LOGIC;
    m_axis_cq_tuser : OUT STD_LOGIC_VECTOR(84 DOWNTO 0);
    m_axis_cq_tvalid : OUT STD_LOGIC;
    s_axis_cc_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_cc_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_cc_tlast : IN STD_LOGIC;
    s_axis_cc_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_cc_tuser : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    s_axis_cc_tvalid : IN STD_LOGIC;
    pcie_rq_seq_num : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie_rq_seq_num_vld : OUT STD_LOGIC;
    pcie_rq_tag : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pcie_rq_tag_av : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    pcie_rq_tag_vld : OUT STD_LOGIC;
    pcie_tfc_nph_av : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    pcie_tfc_npd_av : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    pcie_cq_np_req : IN STD_LOGIC;
    pcie_cq_np_req_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    cfg_phy_link_down : OUT STD_LOGIC;
    cfg_phy_link_status : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_negotiated_width : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_current_speed : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_max_payload : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_max_read_req : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_function_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_function_power_state : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_vf_power_state : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    cfg_link_power_state : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_mgmt_addr : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
    cfg_mgmt_write : IN STD_LOGIC;
    cfg_mgmt_write_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_mgmt_byte_enable : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_mgmt_read : IN STD_LOGIC;
    cfg_mgmt_read_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_mgmt_read_write_done : OUT STD_LOGIC;
    cfg_mgmt_type1_cfg_reg_access : IN STD_LOGIC;
    cfg_err_cor_out : OUT STD_LOGIC;
    cfg_err_nonfatal_out : OUT STD_LOGIC;
    cfg_err_fatal_out : OUT STD_LOGIC;
    cfg_local_error : OUT STD_LOGIC;
    cfg_ltr_enable : OUT STD_LOGIC;
    cfg_ltssm_state : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    cfg_rcb_status : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_dpa_substate_change : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_obff_enable : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_pl_status_change : OUT STD_LOGIC;
    cfg_tph_requester_enable : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_tph_st_mode : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_tph_requester_enable : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_vf_tph_st_mode : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    cfg_msg_received : OUT STD_LOGIC;
    cfg_msg_received_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_msg_received_type : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    cfg_msg_transmit : IN STD_LOGIC;
    cfg_msg_transmit_type : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_msg_transmit_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_msg_transmit_done : OUT STD_LOGIC;
    cfg_fc_ph : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_fc_pd : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_fc_nph : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_fc_npd : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_fc_cplh : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_fc_cpld : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_fc_sel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_per_func_status_control : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_per_func_status_data : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_per_function_number : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_per_function_output_request : IN STD_LOGIC;
    cfg_per_function_update_done : OUT STD_LOGIC;
    cfg_dsn : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    cfg_power_state_change_ack : IN STD_LOGIC;
    cfg_power_state_change_interrupt : OUT STD_LOGIC;
    cfg_err_cor_in : IN STD_LOGIC;
    cfg_err_uncor_in : IN STD_LOGIC;
    cfg_flr_in_process : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_flr_done : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_vf_flr_in_process : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_vf_flr_done : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_link_training_enable : IN STD_LOGIC;
    cfg_interrupt_int : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_pending : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_sent : OUT STD_LOGIC;
    cfg_interrupt_msi_enable : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_msi_vf_enable : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_interrupt_msi_mmenable : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_interrupt_msi_mask_update : OUT STD_LOGIC;
    cfg_interrupt_msi_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_interrupt_msi_select : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_msi_int : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_interrupt_msi_pending_status : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_interrupt_msi_pending_status_data_enable : IN STD_LOGIC;
    cfg_interrupt_msi_pending_status_function_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_msi_sent : OUT STD_LOGIC;
    cfg_interrupt_msi_fail : OUT STD_LOGIC;
    cfg_interrupt_msi_attr : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_interrupt_msi_tph_present : IN STD_LOGIC;
    cfg_interrupt_msi_tph_type : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_interrupt_msi_tph_st_tag : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    cfg_interrupt_msi_function_number : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_hot_reset_out : OUT STD_LOGIC;
    cfg_config_space_enable : IN STD_LOGIC;
    cfg_req_pm_transition_l23_ready : IN STD_LOGIC;
    cfg_hot_reset_in : IN STD_LOGIC;
    cfg_ds_port_number : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_ds_bus_number : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_ds_device_number : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    cfg_ds_function_number : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_subsys_vend_id : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;
    pcie_perstn1_in : IN STD_LOGIC;
    pcie_perstn0_out : OUT STD_LOGIC;
    pcie_perstn1_out : OUT STD_LOGIC;
    int_qpll1lock_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    int_qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    int_qpll1outclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rdy_out : OUT STD_LOGIC
  );
END COMPONENT;


signal    user_clk :  STD_LOGIC;
signal    user_reset :  STD_LOGIC;
 signal   user_lnk_up :  STD_LOGIC;
 signal   s_axis_rq_tdata :  STD_LOGIC_VECTOR(255 DOWNTO 0);
 signal   s_axis_rq_tkeep :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   s_axis_rq_tlast :  STD_LOGIC;
signal    s_axis_rq_tready :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    s_axis_rq_tuser :  STD_LOGIC_VECTOR(59 DOWNTO 0);
 signal   s_axis_rq_tvalid :  STD_LOGIC;
 signal   m_axis_rc_tdata :  STD_LOGIC_VECTOR(255 DOWNTO 0);
signal    m_axis_rc_tkeep :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal    m_axis_rc_tlast :  STD_LOGIC;
signal    m_axis_rc_tready :  STD_LOGIC_VECTOR(21 downto 0);
 signal   m_axis_rc_tuser :  STD_LOGIC_VECTOR(74 DOWNTO 0);
 signal   m_axis_rc_tvalid :  STD_LOGIC;
 signal   m_axis_cq_tdata :  STD_LOGIC_VECTOR(255 DOWNTO 0);
 signal   m_axis_cq_tkeep :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   m_axis_cq_tlast :  STD_LOGIC;
 signal m_axis_cq_tready:  std_logic_vector(21 downto 0);
 signal   m_axis_cq_tuser :  STD_LOGIC_VECTOR(84 DOWNTO 0);
 signal   m_axis_cq_tvalid :  STD_LOGIC;
  signal  s_axis_cc_tdata :  STD_LOGIC_VECTOR(255 DOWNTO 0);
  signal  s_axis_cc_tkeep :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   s_axis_cc_tlast :  STD_LOGIC;
 signal   s_axis_cc_tready :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   s_axis_cc_tuser :  STD_LOGIC_VECTOR(32 DOWNTO 0);
 signal   s_axis_cc_tvalid :  STD_LOGIC;
 signal   pcie_rq_seq_num :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   pcie_rq_seq_num_vld :  STD_LOGIC;
 signal   pcie_rq_tag :  STD_LOGIC_VECTOR(5 DOWNTO 0);
 signal   pcie_rq_tag_av :  STD_LOGIC_VECTOR(1 DOWNTO 0);
 signal   pcie_rq_tag_vld :  STD_LOGIC;
 signal   pcie_tfc_nph_av :  STD_LOGIC_VECTOR(1 DOWNTO 0);
 signal   pcie_tfc_npd_av :  STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal  pcie_cq_np_req :  STD_LOGIC;
  signal  pcie_cq_np_req_count :  STD_LOGIC_VECTOR(5 DOWNTO 0);
 signal   cfg_phy_link_down :  STD_LOGIC;
  signal  cfg_phy_link_status :  STD_LOGIC_VECTOR(1 DOWNTO 0);
 signal   cfg_negotiated_width :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_current_speed :  STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal  cfg_max_payload :  STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal  cfg_max_read_req :  STD_LOGIC_VECTOR(2 DOWNTO 0);
 signal   cfg_function_status :  STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal  cfg_function_power_state :  STD_LOGIC_VECTOR(11 DOWNTO 0);
  signal  cfg_vf_status :  STD_LOGIC_VECTOR(15 DOWNTO 0);
 signal   cfg_vf_power_state :  STD_LOGIC_VECTOR(23 DOWNTO 0);
  signal  cfg_link_power_state :  STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal  cfg_mgmt_addr :  STD_LOGIC_VECTOR(18 DOWNTO 0);
 signal   cfg_mgmt_write :  STD_LOGIC;
 signal   cfg_mgmt_write_data :  STD_LOGIC_VECTOR(31 DOWNTO 0);
 signal   cfg_mgmt_byte_enable :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_mgmt_read :  STD_LOGIC;
 signal   cfg_mgmt_read_data :  STD_LOGIC_VECTOR(31 DOWNTO 0);
 signal   cfg_mgmt_read_write_done :  STD_LOGIC;
 signal   cfg_mgmt_type1_cfg_reg_access :  STD_LOGIC;
 signal   cfg_err_cor_out :  STD_LOGIC;
 signal   cfg_err_nonfatal_out :  STD_LOGIC;
 signal   cfg_err_fatal_out :  STD_LOGIC;
 signal   cfg_local_error :  STD_LOGIC;
 signal   cfg_ltr_enable :  STD_LOGIC;
  signal  cfg_ltssm_state :  STD_LOGIC_VECTOR(5 DOWNTO 0);
 signal   cfg_rcb_status :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_dpa_substate_change :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_obff_enable :  STD_LOGIC_VECTOR(1 DOWNTO 0);
 signal   cfg_pl_status_change :  STD_LOGIC;
 signal   cfg_tph_requester_enable :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_tph_st_mode :  STD_LOGIC_VECTOR(11 DOWNTO 0);
 signal   cfg_vf_tph_requester_enable :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   cfg_vf_tph_st_mode :  STD_LOGIC_VECTOR(23 DOWNTO 0);
 signal   cfg_msg_received :  STD_LOGIC;
 signal   cfg_msg_received_data :  STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal  cfg_msg_received_type :  STD_LOGIC_VECTOR(4 DOWNTO 0);
 signal   cfg_msg_transmit :  STD_LOGIC;
 signal   cfg_msg_transmit_type :  STD_LOGIC_VECTOR(2 DOWNTO 0);
 signal   cfg_msg_transmit_data :  STD_LOGIC_VECTOR(31 DOWNTO 0);
 signal   cfg_msg_transmit_done :  STD_LOGIC;
 signal   cfg_fc_ph :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   cfg_fc_pd :  STD_LOGIC_VECTOR(11 DOWNTO 0);
 signal   cfg_fc_nph :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   cfg_fc_npd :  STD_LOGIC_VECTOR(11 DOWNTO 0);
 signal   cfg_fc_cplh :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   cfg_fc_cpld :  STD_LOGIC_VECTOR(11 DOWNTO 0);
 signal   cfg_fc_sel :  STD_LOGIC_VECTOR(2 DOWNTO 0);
 signal   cfg_per_func_status_control :  STD_LOGIC_VECTOR(2 DOWNTO 0);
 signal   cfg_per_func_status_data :  STD_LOGIC_VECTOR(15 DOWNTO 0);
 signal   cfg_per_function_number :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_per_function_output_request :  STD_LOGIC;
 signal   cfg_per_function_update_done :  STD_LOGIC;
 signal   cfg_dsn :  STD_LOGIC_VECTOR(63 DOWNTO 0);
signal    cfg_power_state_change_ack :  STD_LOGIC;
signal    cfg_power_state_change_interrupt :  STD_LOGIC;
 signal   cfg_err_cor_in :  STD_LOGIC;
signal    cfg_err_uncor_in :  STD_LOGIC;
 signal   cfg_flr_in_process :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_flr_done :  STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
signal    cfg_vf_flr_in_process :  STD_LOGIC_VECTOR(7 DOWNTO 0);
 signal   cfg_vf_flr_done :  STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
 signal   cfg_link_training_enable :  STD_LOGIC;
signal    cfg_interrupt_int :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_interrupt_pending :  STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
 signal   cfg_interrupt_sent :  STD_LOGIC;
 signal   cfg_interrupt_msi_enable :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_interrupt_msi_vf_enable :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal    cfg_interrupt_msi_mmenable :  STD_LOGIC_VECTOR(11 DOWNTO 0);
signal    cfg_interrupt_msi_mask_update :  STD_LOGIC;
 signal   cfg_interrupt_msi_data :  STD_LOGIC_VECTOR(31 DOWNTO 0);
signal    cfg_interrupt_msi_select :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   cfg_interrupt_msi_int :  STD_LOGIC_VECTOR(31 DOWNTO 0);
signal    cfg_interrupt_msi_pending_status :  STD_LOGIC_VECTOR(31 DOWNTO 0);
signal    cfg_interrupt_msi_pending_status_data_enable :  STD_LOGIC;
 signal   cfg_interrupt_msi_pending_status_function_num :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_interrupt_msi_sent :  STD_LOGIC;
signal    cfg_interrupt_msi_fail :  STD_LOGIC;
signal    cfg_interrupt_msi_attr :  STD_LOGIC_VECTOR(2 DOWNTO 0);
signal    cfg_interrupt_msi_tph_present :  STD_LOGIC;
signal    cfg_interrupt_msi_tph_type :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    cfg_interrupt_msi_tph_st_tag :  STD_LOGIC_VECTOR(8 DOWNTO 0);
signal    cfg_interrupt_msi_function_number :  STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
signal    cfg_hot_reset_out :  STD_LOGIC;
 signal   cfg_config_space_enable :  STD_LOGIC;
 signal   cfg_req_pm_transition_l23_ready :  STD_LOGIC;
signal    cfg_hot_reset_in :  STD_LOGIC;
signal    cfg_ds_port_number :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal    cfg_ds_bus_number :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal    cfg_ds_device_number :  STD_LOGIC_VECTOR(4 DOWNTO 0);
signal    cfg_ds_function_number :  STD_LOGIC_VECTOR(2 DOWNTO 0);
signal    cfg_subsys_vend_id :  STD_LOGIC_VECTOR(15 DOWNTO 0);
signal    sys_clk :  STD_LOGIC;
signal    sys_clk_gt :  STD_LOGIC;
signal    sys_reset :  STD_LOGIC;
signal    pcie_perstn1_in :  STD_LOGIC;
signal    pcie_perstn0_out :  STD_LOGIC;
signal    pcie_perstn1_out :  STD_LOGIC;
signal    int_qpll1lock_out :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    int_qpll1outrefclk_out :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    int_qpll1outclk_out :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    phy_rdy_out :  STD_LOGIC;

 COMPONENT pcie_app_uscale is
 generic (
    C_DATA_WIDTH: natural:= 256; --            // RX/TX interface data width 
    KEEP_WIDTH: natural:=8
 );
 port (
  ------------------------------------------------------------------------------------------------------------------//
  --  AXI Interface                                                                                                 //
  ------------------------------------------------------------------------------------------------------------------//
  s_axis_rq_tlast: out std_logic;
  s_axis_rq_tdata: out std_logic_vector(C_DATA_WIDTH - 1 downto 0);
  s_axis_rq_tuser: out std_logic_vector(59 downto 0);
  s_axis_rq_tkeep: out std_logic_vector(KEEP_WIDTH-1 downto 0);
  s_axis_rq_tready: in std_logic_vector(3 downto 0);
  s_axis_rq_tvalid: out std_logic;
  
  m_axis_rc_tdata: in std_logic_vector(C_DATA_WIDTH - 1 downto 0);
  m_axis_rc_tuser: in std_logic_vector(74 downto 0);
  m_axis_rc_tlast: in std_logic;
  m_axis_rc_tkeep: in  std_logic_vector(KEEP_WIDTH - 1 downto 0);
  m_axis_rc_tvalid: in std_logic;
  m_axis_rc_tready: out std_logic_vector(21 downto 0);

  m_axis_cq_tdata: in std_logic_vector(C_DATA_WIDTH - 1 downto 0);
  m_axis_cq_tuser: in std_logic_vector(84 downto 0);
  m_axis_cq_tlast: in std_logic;
  m_axis_cq_tkeep: in std_logic_vector(KEEP_WIDTH - 1 downto 0);
  m_axis_cq_tvalid: in std_logic;
  m_axis_cq_tready: out std_logic_vector(21 downto 0);
  
  s_axis_cc_tdata: out std_logic_vector(C_DATA_WIDTH - 1 downto 0);
  s_axis_cc_tuser: out std_logic_vector(32 downto 0);
  s_axis_cc_tlast: out std_logic;
  s_axis_cc_tkeep: out std_logic_vector(KEEP_WIDTH - 1 downto 0);
  s_axis_cc_tvalid: out std_logic;
  s_axis_cc_tready: in std_logic_vector(3 downto 0);
  
  pcie_tfc_nph_av: std_logic_vector(1 downto 0);
  pcie_tfc_npd_av: std_logic_vector(1 downto 0);
  
  ------------------------------------------------------------------------------------------------------------------//
  --  Configuration (CFG) Interface                                                                                 //
  ------------------------------------------------------------------------------------------------------------------//

  pcie_rq_seq_num: in std_logic_vector(3 downto 0);
  pcie_rq_seq_num_vld: in std_logic;
  pcie_rq_tag: in std_logic_vector(5 downto 0);
  pcie_rq_tag_vld: in std_logic;
  pcie_cq_np_req: out std_logic;
  pcie_cq_np_req_count: in std_logic_vector(5 downto 0);
  
  ------------------------------------------------------------------------------------------------------------------//
  -- EP and RP                                                                                                      //
  ------------------------------------------------------------------------------------------------------------------//
  
  cfg_phy_link_down: in std_logic;
  cfg_negotiated_width: in std_logic_vector(3 downto 0);
  cfg_current_speed: in std_logic_vector(2 downto 0);
  cfg_max_payload: in std_logic_vector(2 downto 0);
  cfg_max_read_req: in std_logic_vector(2 downto 0);
  cfg_function_status: in std_logic_vector(7 downto 0);
  cfg_function_power_state: in std_logic_vector(5 downto 0);
  cfg_vf_status: in std_logic_vector(11 downto 0);
  cfg_vf_power_state: in std_logic_vector(17 downto 0);
  cfg_link_power_state: in std_logic_vector(1 downto 0);

  -- Error Reporting Interface
  cfg_err_cor_out: in std_logic;
  cfg_err_nonfatal_out: in std_logic;
  cfg_err_fatal_out: in std_logic;
  --cfg_local_error: in std_logic
  
  cfg_ltr_enable: in std_logic;
  cfg_ltssm_state: in std_logic_vector(5 downto 0);
  cfg_rcb_status: in std_logic_vector(1 downto 0);
  cfg_dpa_substate_change: in std_logic_vector(1 downto 0);
  cfg_obff_enable: in std_logic_vector(1 downto 0);
  cfg_pl_status_change: in std_logic;

  cfg_tph_requester_enable: in std_logic_vector(1 downto 0);
  cfg_tph_st_mode: in std_logic_vector(5 downto 0);
  cfg_vf_tph_requester_enable: in std_logic_vector(5 downto 0);
  cfg_vf_tph_st_mode: in std_logic_vector(17 downto 0);

  -- Management Interface
  cfg_mgmt_addr: out std_logic_vector(18 downto 0);
  cfg_mgmt_write: out std_logic;
  cfg_mgmt_write_data: out std_logic_vector(31 downto 0);
  cfg_mgmt_byte_enable: out std_logic_vector(3 downto 0);
  cfg_mgmt_read: out std_logic;
  cfg_mgmt_read_data: in std_logic_vector(31 downto 0);
  cfg_mgmt_read_write_done: in std_logic;
  cfg_mgmt_type1_cfg_reg_access: out std_logic;
  cfg_msg_received: in std_logic;
  cfg_msg_received_data: in std_logic_vector(7 downto 0);
  cfg_msg_received_type: in std_logic_vector(4 downto 0);
  cfg_msg_transmit: out std_logic;
  cfg_msg_transmit_type: out std_logic_vector(2 downto 0);
  cfg_msg_transmit_data: out std_logic_vector(31 downto 0);
  cfg_msg_transmit_done: in std_logic;

  
  cfg_fc_ph: in std_logic_vector(7 downto 0);
  cfg_fc_pd: in std_logic_vector(11 downto 0);
  cfg_fc_nph: in std_logic_vector(7 downto 0);
  cfg_fc_npd: in std_logic_vector(11 downto 0);
  cfg_fc_cplh: in std_logic_vector(7 downto 0);
  cfg_fc_cpld: in std_logic_vector(11 downto 0);
  cfg_fc_sel: out std_logic_vector(2 downto 0);
  cfg_per_func_status_control: out std_logic_vector(2 downto 0);
  cfg_per_func_status_data: in std_logic_vector(15 downto 0);
  cfg_per_function_number: out std_logic_vector(3 downto 0);
  cfg_per_function_output_request: out std_logic;
  cfg_per_function_update_done: in std_logic;
  
  cfg_dsn: out std_logic_vector(63 downto 0);
  cfg_power_state_change_ack: out std_logic;
  cfg_power_state_change_interrupt: in std_logic;
  cfg_err_cor_in: out std_logic;
  cfg_err_uncor_in: out std_logic;

  cfg_flr_in_process: in  std_logic_vector(1 downto 0);
  cfg_flr_done: out  std_logic_vector(1 downto 0);
  cfg_vf_flr_in_process: in  std_logic_vector(5 downto 0);
  cfg_vf_flr_done: out  std_logic_vector(5 downto 0);
  
  cfg_link_training_enable: out std_logic;

  cfg_ds_port_number: out std_logic_vector(7 downto 0);
  cfg_ds_bus_number: out std_logic_vector(7 downto 0);
  cfg_ds_device_number: out std_logic_vector(4 downto 0);
  cfg_ds_function_number: out std_logic_vector(2 downto 0);
  
  ------------------------------------------------------------------------------------------------------------------//
  -- EP Only                                                                                                        //
  ------------------------------------------------------------------------------------------------------------------//

  -- Interrupt Interface Signals
  cfg_interrupt_int: out std_logic_vector(3 downto 0);
  cfg_interrupt_pending: out std_logic_vector(1 downto 0);
  cfg_interrupt_sent: in std_logic;

  cfg_interrupt_msi_enable: in std_logic_vector(1 downto 0);
  cfg_interrupt_msi_vf_enable: in std_logic_vector(5 downto 0);
  cfg_interrupt_msi_mmenable: in std_logic_vector(5 downto 0);
  cfg_interrupt_msi_mask_update: in std_logic;
  cfg_interrupt_msi_data: in std_logic_vector(31 downto 0);
  cfg_interrupt_msi_select: out std_logic_vector(3 downto 0);
  cfg_interrupt_msi_int: out std_logic_vector(31 downto 0);
  cfg_interrupt_msi_pending_status: out std_logic_vector(31 downto 0);
  cfg_interrupt_msi_sent: in std_logic;
  cfg_interrupt_msi_fail: in std_logic;
  cfg_interrupt_msi_attr: out std_logic_vector(2 downto 0);
  cfg_interrupt_msi_tph_present: out std_logic;
  cfg_interrupt_msi_tph_type: out std_logic_vector(1 downto 0);
  cfg_interrupt_msi_tph_st_tag: out std_logic_vector(8 downto 0);
  cfg_interrupt_msi_function_number: out std_logic_vector(2 downto 0);
  
  
  --  EP only
  cfg_hot_reset_in: in std_logic;
  cfg_config_space_enable: out std_logic;
  cfg_req_pm_transition_l23_ready: out std_logic;
  
  -- RP only
  cfg_hot_reset_out: out std_logic;
  
  led_out: out std_logic_vector(7 downto 0);

  user_clk: in std_logic;
  user_reset: in std_logic;
  user_lnk_up: in std_logic;
  sys_rst_n: in std_logic
);
 END COMPONENT;

 
begin

pcie3_ultrascale_0_i : pcie3_ultrascale_0
  PORT MAP (
    pci_exp_txn => pci_exp_txn,
    pci_exp_txp => pci_exp_txp,
    pci_exp_rxn => pci_exp_rxn,
    pci_exp_rxp => pci_exp_rxp,
    user_clk => user_clk,
    user_reset => user_reset,
    user_lnk_up => user_lnk_up,
    s_axis_rq_tdata => s_axis_rq_tdata,
    s_axis_rq_tkeep => s_axis_rq_tkeep,
    s_axis_rq_tlast => s_axis_rq_tlast,
    s_axis_rq_tready => s_axis_rq_tready,
    s_axis_rq_tuser => s_axis_rq_tuser,
    s_axis_rq_tvalid => s_axis_rq_tvalid,
    m_axis_rc_tdata => m_axis_rc_tdata,
    m_axis_rc_tkeep => m_axis_rc_tkeep,
    m_axis_rc_tlast => m_axis_rc_tlast,
    m_axis_rc_tready => m_axis_rc_tready(0),
    m_axis_rc_tuser => m_axis_rc_tuser,
    m_axis_rc_tvalid => m_axis_rc_tvalid,
    
--    m_axis_cq_tdata  => m_axis_cq_tdata,
--    m_axis_cq_tkeep  => m_axis_cq_tkeep,
--    m_axis_cq_tlast  => m_axis_cq_tlast,
--    m_axis_cq_tready => m_axis_cq_tready(0),
--    m_axis_cq_tuser  => m_axis_cq_tuser,
--    m_axis_cq_tvalid => m_axis_cq_tvalid,
    
--    s_axis_cc_tdata  => s_axis_cc_tdata,
--    s_axis_cc_tkeep  => s_axis_cc_tkeep,
--    s_axis_cc_tlast  => s_axis_cc_tlast,
--    s_axis_cc_tready => s_axis_cc_tready,
--    s_axis_cc_tuser  => s_axis_cc_tuser,
--    s_axis_cc_tvalid => s_axis_cc_tvalid,
    
    m_axis_cq_tdata  => s_axis_cq_m2s.tdata,
    m_axis_cq_tkeep  => s_axis_cq_m2s.tkeep,
    m_axis_cq_tlast  => s_axis_cq_m2s.tlast,
    m_axis_cq_tready => s_axis_cq_s2m.tready,
    m_axis_cq_tuser  => s_axis_cq_m2s.tuser,
    m_axis_cq_tvalid => s_axis_cq_m2s.tvalid,
    
    s_axis_cc_tdata  => m_axis_cc_m2s.tdata,
    s_axis_cc_tkeep  => m_axis_cc_m2s.tkeep,
    s_axis_cc_tlast  => m_axis_cc_m2s.tlast,
    s_axis_cc_tready => m_axis_cc_s2m_tready,
    s_axis_cc_tuser  => m_axis_cc_m2s.tuser,
    s_axis_cc_tvalid => m_axis_cc_m2s.tvalid,
    
    pcie_rq_seq_num => pcie_rq_seq_num,
    pcie_rq_seq_num_vld => pcie_rq_seq_num_vld,
    pcie_rq_tag => pcie_rq_tag,
    pcie_rq_tag_av => pcie_rq_tag_av,
    pcie_rq_tag_vld => pcie_rq_tag_vld,
    pcie_tfc_nph_av => pcie_tfc_nph_av,
    pcie_tfc_npd_av => pcie_tfc_npd_av,
    pcie_cq_np_req => pcie_cq_np_req,
    pcie_cq_np_req_count => pcie_cq_np_req_count,
    cfg_phy_link_down => cfg_phy_link_down,
    cfg_phy_link_status => cfg_phy_link_status,
    cfg_negotiated_width => cfg_negotiated_width,
    cfg_current_speed => cfg_current_speed,
    cfg_max_payload => cfg_max_payload,
    cfg_max_read_req => cfg_max_read_req,
    cfg_function_status => cfg_function_status,
    cfg_function_power_state => cfg_function_power_state,
    cfg_vf_status => cfg_vf_status,
    cfg_vf_power_state => cfg_vf_power_state,
    cfg_link_power_state => cfg_link_power_state,
    cfg_mgmt_addr => cfg_mgmt_addr,
    cfg_mgmt_write => cfg_mgmt_write,
    cfg_mgmt_write_data => cfg_mgmt_write_data,
    cfg_mgmt_byte_enable => cfg_mgmt_byte_enable,
    cfg_mgmt_read => cfg_mgmt_read,
    cfg_mgmt_read_data => cfg_mgmt_read_data,
    cfg_mgmt_read_write_done => cfg_mgmt_read_write_done,
    cfg_mgmt_type1_cfg_reg_access => cfg_mgmt_type1_cfg_reg_access,
    cfg_err_cor_out => cfg_err_cor_out,
    cfg_err_nonfatal_out => cfg_err_nonfatal_out,
    cfg_err_fatal_out => cfg_err_fatal_out,
    cfg_local_error => cfg_local_error,
    cfg_ltr_enable => cfg_ltr_enable,
    cfg_ltssm_state => cfg_ltssm_state,
    cfg_rcb_status => cfg_rcb_status,
    cfg_dpa_substate_change => cfg_dpa_substate_change,
    cfg_obff_enable => cfg_obff_enable,
    cfg_pl_status_change => cfg_pl_status_change,
    cfg_tph_requester_enable => cfg_tph_requester_enable,
    cfg_tph_st_mode => cfg_tph_st_mode,
    cfg_vf_tph_requester_enable => cfg_vf_tph_requester_enable,
    cfg_vf_tph_st_mode => cfg_vf_tph_st_mode,
    cfg_msg_received => cfg_msg_received,
    cfg_msg_received_data => cfg_msg_received_data,
    cfg_msg_received_type => cfg_msg_received_type,
    cfg_msg_transmit => cfg_msg_transmit,
    cfg_msg_transmit_type => cfg_msg_transmit_type,
    cfg_msg_transmit_data => cfg_msg_transmit_data,
    cfg_msg_transmit_done => cfg_msg_transmit_done,
    cfg_fc_ph => cfg_fc_ph,
    cfg_fc_pd => cfg_fc_pd,
    cfg_fc_nph => cfg_fc_nph,
    cfg_fc_npd => cfg_fc_npd,
    cfg_fc_cplh => cfg_fc_cplh,
    cfg_fc_cpld => cfg_fc_cpld,
    cfg_fc_sel => cfg_fc_sel,
    cfg_per_func_status_control => cfg_per_func_status_control,
    cfg_per_func_status_data => cfg_per_func_status_data,
    cfg_per_function_number => cfg_per_function_number,
    cfg_per_function_output_request => cfg_per_function_output_request,
    cfg_per_function_update_done => cfg_per_function_update_done,
    cfg_dsn => cfg_dsn,
    cfg_power_state_change_ack => cfg_power_state_change_ack,
    cfg_power_state_change_interrupt => cfg_power_state_change_interrupt,
    cfg_err_cor_in => cfg_err_cor_in,
    cfg_err_uncor_in => cfg_err_uncor_in,
    cfg_flr_in_process => cfg_flr_in_process,
    cfg_flr_done => cfg_flr_done,
    cfg_vf_flr_in_process => cfg_vf_flr_in_process,
    cfg_vf_flr_done => cfg_vf_flr_done,
    cfg_link_training_enable => cfg_link_training_enable,
    cfg_interrupt_int => cfg_interrupt_int,
    cfg_interrupt_pending => cfg_interrupt_pending,
    cfg_interrupt_sent => cfg_interrupt_sent,
    cfg_interrupt_msi_enable => cfg_interrupt_msi_enable,
    cfg_interrupt_msi_vf_enable => cfg_interrupt_msi_vf_enable,
    cfg_interrupt_msi_mmenable => cfg_interrupt_msi_mmenable,
    cfg_interrupt_msi_mask_update => cfg_interrupt_msi_mask_update,
    cfg_interrupt_msi_data => cfg_interrupt_msi_data,
    cfg_interrupt_msi_select => cfg_interrupt_msi_select,
    cfg_interrupt_msi_int => cfg_interrupt_msi_int,
    cfg_interrupt_msi_pending_status => cfg_interrupt_msi_pending_status,
    cfg_interrupt_msi_pending_status_data_enable => cfg_interrupt_msi_pending_status_data_enable,
    cfg_interrupt_msi_pending_status_function_num => cfg_interrupt_msi_pending_status_function_num,
    cfg_interrupt_msi_sent => cfg_interrupt_msi_sent,
    cfg_interrupt_msi_fail => cfg_interrupt_msi_fail,
    cfg_interrupt_msi_attr => cfg_interrupt_msi_attr,
    cfg_interrupt_msi_tph_present => cfg_interrupt_msi_tph_present,
    cfg_interrupt_msi_tph_type => cfg_interrupt_msi_tph_type,
    cfg_interrupt_msi_tph_st_tag => cfg_interrupt_msi_tph_st_tag,
    cfg_interrupt_msi_function_number => cfg_interrupt_msi_function_number,
    cfg_hot_reset_out => cfg_hot_reset_out,
    cfg_config_space_enable => cfg_config_space_enable,
    cfg_req_pm_transition_l23_ready => cfg_req_pm_transition_l23_ready,
    cfg_hot_reset_in => cfg_hot_reset_in,
    cfg_ds_port_number => cfg_ds_port_number,
    cfg_ds_bus_number => cfg_ds_bus_number,
    cfg_ds_device_number => cfg_ds_device_number,
    cfg_ds_function_number => cfg_ds_function_number,
    cfg_subsys_vend_id => cfg_subsys_vend_id,
    sys_clk => sys_clk,
    sys_clk_gt => sys_clk_gt,
    sys_reset => sys_reset,
    pcie_perstn1_in => pcie_perstn1_in,
    pcie_perstn0_out => pcie_perstn0_out,
    pcie_perstn1_out => pcie_perstn1_out,
    int_qpll1lock_out => int_qpll1lock_out,
    int_qpll1outrefclk_out => int_qpll1outrefclk_out,
    int_qpll1outclk_out => int_qpll1outclk_out,
    phy_rdy_out => phy_rdy_out
  );
--  s_axis_cq_s2m.tready <=  s_axis_cq_s2m_tready(0);
  m_axis_cc_s2m.tready <= m_axis_cc_s2m_tready(0);
  m_axis_cq_s2m(0).tready <= m_axis_cq_s2m_0_tready(0);
  s_axis_cc_s2m_0_tready <= (others => s_axis_cc_s2m(0).tready);
  
  pcie_app_uscale_i:  pcie_app_uscale  
  port map (
    user_clk              =>                          user_clk ,
    user_reset              =>                        user_reset ,
    user_lnk_up               =>                      user_lnk_up ,
    sys_rst_n                   =>                    sys_rst_n_c ,
    led_out                       =>                  led_out,

    ---------------------------------------------------------------------------------------//
    --  AXI Interface                                                                      //
    ---------------------------------------------------------------------------------------//

    s_axis_rq_tlast                 =>                s_axis_rq_tlast ,
    s_axis_rq_tdata                   =>              s_axis_rq_tdata ,
    s_axis_rq_tuser                     =>            s_axis_rq_tuser ,
    s_axis_rq_tkeep                       =>          s_axis_rq_tkeep ,
    s_axis_rq_tready                        =>        (others => '0'), --s_axis_rq_tready ,
    s_axis_rq_tvalid                          =>      s_axis_rq_tvalid ,

    m_axis_rc_tdata     =>                            m_axis_rc_tdata ,
    m_axis_rc_tuser       =>                          m_axis_rc_tuser ,
    m_axis_rc_tlast         =>                        m_axis_rc_tlast ,
    m_axis_rc_tkeep           =>                      m_axis_rc_tkeep ,
    m_axis_rc_tvalid            =>                    m_axis_rc_tvalid ,
    m_axis_rc_tready              =>                  m_axis_rc_tready ,

--    m_axis_cq_tdata     =>                            m_axis_cq_tdata ,
--    m_axis_cq_tuser       =>                          m_axis_cq_tuser,
--    m_axis_cq_tlast         =>                        m_axis_cq_tlast ,
--    m_axis_cq_tkeep           =>                      m_axis_cq_tkeep ,
--    m_axis_cq_tvalid            =>                    m_axis_cq_tvalid ,
--    m_axis_cq_tready           =>                     m_axis_cq_tready ,

--    s_axis_cc_tdata                 =>                s_axis_cc_tdata,
--    s_axis_cc_tuser                   =>              s_axis_cc_tuser ,
--    s_axis_cc_tlast                     =>            s_axis_cc_tlast ,
--    s_axis_cc_tkeep                       =>          s_axis_cc_tkeep ,
--    s_axis_cc_tvalid                        =>        s_axis_cc_tvalid ,
--    s_axis_cc_tready   =>                             s_axis_cc_tready,
    
--    m_axis_cq_tdata     =>                            s_axis_cq_m2s.tdata ,
--    m_axis_cq_tuser       =>                          s_axis_cq_m2s.tuser,
--    m_axis_cq_tlast         =>                        s_axis_cq_m2s.tlast ,
--    m_axis_cq_tkeep           =>                      s_axis_cq_m2s.tkeep ,
--    m_axis_cq_tvalid            =>                    s_axis_cq_m2s.tvalid ,
--    m_axis_cq_tready           =>                     s_axis_cq_s2m_tready ,
--
--    s_axis_cc_tdata                 =>                m_axis_cc_m2s.tdata,
--    s_axis_cc_tuser                   =>              m_axis_cc_m2s.tuser ,
--    s_axis_cc_tlast                     =>            m_axis_cc_m2s.tlast ,
--    s_axis_cc_tkeep                       =>          m_axis_cc_m2s.tkeep ,
--    s_axis_cc_tvalid                        =>        m_axis_cc_m2s.tvalid ,
--    s_axis_cc_tready   =>                             m_axis_cc_s2m_tready,
    
    m_axis_cq_tdata     =>                            m_axis_cq_m2s(0).tdata ,
    m_axis_cq_tuser       =>                          m_axis_cq_m2s(0).tuser,
    m_axis_cq_tlast         =>                        m_axis_cq_m2s(0).tlast ,
    m_axis_cq_tkeep           =>                      m_axis_cq_m2s(0).tkeep ,
    m_axis_cq_tvalid            =>                    m_axis_cq_m2s(0).tvalid ,
    m_axis_cq_tready           =>                     m_axis_cq_s2m_0_tready ,

    s_axis_cc_tdata                 =>                s_axis_cc_m2s(0).tdata,
    s_axis_cc_tuser                   =>              s_axis_cc_m2s(0).tuser ,
    s_axis_cc_tlast                     =>            s_axis_cc_m2s(0).tlast ,
    s_axis_cc_tkeep                       =>          s_axis_cc_m2s(0).tkeep ,
    s_axis_cc_tvalid                        =>        s_axis_cc_m2s(0).tvalid ,
    s_axis_cc_tready   =>                             s_axis_cc_s2m_0_tready,

    pcie_tfc_nph_av   =>                              pcie_tfc_nph_av ,
    pcie_tfc_npd_av     =>                            pcie_tfc_npd_av ,

    pcie_rq_seq_num       =>                          pcie_rq_seq_num ,
    pcie_rq_seq_num_vld     =>                        pcie_rq_seq_num_vld ,
    pcie_rq_tag               =>                      pcie_rq_tag,
    pcie_rq_tag_vld             =>                    pcie_rq_tag_vld ,
    pcie_cq_np_req_count          =>                  pcie_cq_np_req_count ,
    pcie_cq_np_req                  =>                pcie_cq_np_req ,

    ----------------------------------------------------------------------------------//
    --  Configuration (CFG) Interface                                                 //
    ----------------------------------------------------------------------------------//

    ----------------------------------------------------------------------------------//
    -- EP and RP                                                                      //
    ----------------------------------------------------------------------------------//

    cfg_phy_link_down            =>                   cfg_phy_link_down ,
    cfg_negotiated_width           =>                 cfg_negotiated_width ,
    cfg_current_speed                =>               cfg_current_speed ,
    cfg_max_payload                    =>             cfg_max_payload ,
    cfg_max_read_req                     =>           cfg_max_read_req ,
    cfg_function_status                    =>         cfg_function_status(7 downto 0),
    cfg_function_power_state                 =>       cfg_function_power_state(5 downto 0) ,
    cfg_vf_status                              =>     cfg_vf_status(11 downto 0),
    cfg_vf_power_state               =>               cfg_vf_power_state(17 downto 0),
    cfg_link_power_state           =>                 cfg_link_power_state ,

    -- Error Reporting Interface =>=>
    cfg_err_cor_out                 =>                cfg_err_cor_out ,
    cfg_err_nonfatal_out          =>                  cfg_err_nonfatal_out ,
    cfg_err_fatal_out           =>                    cfg_err_fatal_out ,

    cfg_ltr_enable                        =>          cfg_ltr_enable ,
    cfg_ltssm_state                    =>             cfg_ltssm_state ,
    cfg_rcb_status                   =>               cfg_rcb_status(1 downto 0),
    cfg_dpa_substate_change        =>                 cfg_dpa_substate_change (1 downto 0) ,
    cfg_obff_enable                          =>       cfg_obff_enable ,
    cfg_pl_status_change         =>                   cfg_pl_status_change ,

    cfg_tph_requester_enable               =>         cfg_tph_requester_enable (1 downto 0) ,
    cfg_tph_st_mode                      =>           cfg_tph_st_mode (5 downto 0),
    cfg_vf_tph_requester_enable        =>             cfg_vf_tph_requester_enable (5 downto 0) ,
    cfg_vf_tph_st_mode               =>               cfg_vf_tph_st_mode (17 downto 0),

    -- Management Interface
    cfg_mgmt_addr                  =>                 cfg_mgmt_addr ,
    cfg_mgmt_write               =>                   cfg_mgmt_write ,
    cfg_mgmt_write_data        =>                     cfg_mgmt_write_data ,
    cfg_mgmt_byte_enable     =>                       cfg_mgmt_byte_enable ,
    cfg_mgmt_read                                =>   cfg_mgmt_read ,
    cfg_mgmt_read_data                         =>     cfg_mgmt_read_data ,
    cfg_mgmt_read_write_done                 =>       cfg_mgmt_read_write_done ,
    cfg_mgmt_type1_cfg_reg_access          =>         cfg_mgmt_type1_cfg_reg_access ,
    cfg_msg_received                    =>            cfg_msg_received ,
    cfg_msg_received_data             =>              cfg_msg_received_data ,
    cfg_msg_received_type           =>                cfg_msg_received_type ,
    cfg_msg_transmit              =>                 cfg_msg_transmit ,
    cfg_msg_transmit_type      =>                     cfg_msg_transmit_type ,
    cfg_msg_transmit_data    =>                       cfg_msg_transmit_data,
    cfg_msg_transmit_done  =>                          cfg_msg_transmit_done,

    cfg_fc_ph                      =>                 cfg_fc_ph ,
    cfg_fc_pd                              =>         cfg_fc_pd ,
    cfg_fc_nph                           =>           cfg_fc_nph ,
    cfg_fc_npd                             =>         cfg_fc_npd ,
    cfg_fc_cplh                          =>           cfg_fc_cplh ,
    cfg_fc_cpld                        =>             cfg_fc_cpld ,
    cfg_fc_sel                       =>               cfg_fc_sel ,

    cfg_per_func_status_control    =>                 cfg_per_func_status_control ,
    cfg_per_func_status_data                        => cfg_per_func_status_data ,
    cfg_per_function_number                       =>  cfg_per_function_number ,
    cfg_per_function_output_request             =>    cfg_per_function_output_request ,
    cfg_per_function_update_done              =>      cfg_per_function_update_done ,

    cfg_dsn                                 =>        cfg_dsn ,
    cfg_power_state_change_ack            =>          cfg_power_state_change_ack ,
    cfg_power_state_change_interrupt    =>            cfg_power_state_change_interrupt ,
    cfg_err_cor_in                    =>              cfg_err_cor_in ,
    cfg_err_uncor_in                =>                cfg_err_uncor_in ,

    cfg_flr_in_process                =>              cfg_flr_in_process(1 downto 0),
    cfg_flr_done                   =>                 cfg_flr_done(1 downto 0),
    cfg_vf_flr_in_process        =>                   cfg_vf_flr_in_process (5 downto 0),
    cfg_vf_flr_done            =>                     cfg_vf_flr_done(5 downto 0), 

    cfg_link_training_enable =>                       cfg_link_training_enable ,

    cfg_ds_port_number     =>                         cfg_ds_port_number ,
    cfg_hot_reset_in     =>                           cfg_hot_reset_out ,
    cfg_config_space_enable                   =>      cfg_config_space_enable ,
    cfg_req_pm_transition_l23_ready         =>        cfg_req_pm_transition_l23_ready ,

  -- RP only
    cfg_hot_reset_out                     =>          cfg_hot_reset_in ,

    cfg_ds_bus_number                   =>            cfg_ds_bus_number ,
    cfg_ds_device_number              =>             cfg_ds_device_number ,
    cfg_ds_function_number          =>                cfg_ds_function_number ,


    ---------------------------------------------------------------------------------------//
    -- EP Only                                                                             //
    ---------------------------------------------------------------------------------------//

    cfg_interrupt_msi_enable         =>               cfg_interrupt_msi_enable(1 downto 0),
    cfg_interrupt_msi_vf_enable    =>                 cfg_interrupt_msi_vf_enable(5 downto 0),
    cfg_interrupt_msi_mmenable                   =>   cfg_interrupt_msi_mmenable(5 downto 0),
    cfg_interrupt_msi_mask_update              =>     cfg_interrupt_msi_mask_update ,
    cfg_interrupt_msi_data                   =>       cfg_interrupt_msi_data ,
    cfg_interrupt_msi_select               =>         cfg_interrupt_msi_select ,
    cfg_interrupt_msi_int                =>          cfg_interrupt_msi_int,
    cfg_interrupt_msi_pending_status   =>             cfg_interrupt_msi_pending_status ,
    cfg_interrupt_msi_sent           =>               cfg_interrupt_msi_sent ,
    cfg_interrupt_msi_fail                    =>      cfg_interrupt_msi_fail ,
    cfg_interrupt_msi_attr                  =>        cfg_interrupt_msi_attr ,
    cfg_interrupt_msi_tph_present         =>          cfg_interrupt_msi_tph_present ,
    cfg_interrupt_msi_tph_type          =>           cfg_interrupt_msi_tph_type ,
    cfg_interrupt_msi_tph_st_tag         =>          cfg_interrupt_msi_tph_st_tag ,
    cfg_interrupt_msi_function_number  =>             cfg_interrupt_msi_function_number(2 downto 0) ,

    -- Interrupt Interface Signals
    cfg_interrupt_int                =>               cfg_interrupt_int ,
    cfg_interrupt_pending          =>                 cfg_interrupt_pending(1 downto 0) ,
    cfg_interrupt_sent             =>               cfg_interrupt_sent 
  );
    

--m_axis_cq_m2s(0) <= s_axis_cq_m2s;
--s_axis_cq_s2m <= m_axis_cq_s2m(0);
--m_axis_cc_m2s <= s_axis_cc_m2s(0);
--s_axis_cc_s2m(0) <= m_axis_cc_s2m;

u_tlp_interconnect: pcie3_tlp_interconnect
    generic map(
      g_routing => c_tlp_routing,
      g_fallback => 0
    )
    Port map( clk_i => user_clk,
           rst_n_i => user_reset_n,
           
           s_axis_cq_m2s => s_axis_cq_m2s,
           s_axis_cq_s2m => s_axis_cq_s2m,
           
           m_axis_cc_m2s => m_axis_cc_m2s,
           m_axis_cc_s2m => m_axis_cc_s2m,
           
           m_axis_cq_m2s => m_axis_cq_m2s,
           m_axis_cq_s2m => m_axis_cq_s2m,
           
           s_axis_cc_m2s => s_axis_cc_m2s,
           s_axis_cc_s2m => s_axis_cc_s2m
);


b_tlp_to_wb: block
  signal m_wb_m2s: t_wishbone_a32_d32_m2s;
  signal m_wb_s2m: t_wishbone_a32_d32_s2m;
begin

u_tlp_to_wb: pcie3_tlp_to_wb 
  Port map (
         clk_i  => user_clk,
         rst_n_i => user_reset_n,
         clk2_i => user_clk,
         rst2_n_i => user_reset_n,
           
         s_axis_cq_m2s => m_axis_cq_m2s(1),
         s_axis_cq_s2m => m_axis_cq_s2m(1), 
         
         m_axis_cc_m2s => s_axis_cc_m2s(1),
         m_axis_cc_s2m => s_axis_cc_s2m(1),
                             
           -- Wishbone master interface
         m_wb_m2s => m_wb_m2s,
         m_wb_s2m => m_wb_s2m
  );

  u_dummy: wb_dummy_slave 
    Port map (clk_i  => user_clk,
         rst_n_i => user_reset_n,
               -- Wishbone master interface
           m_wb_m2s => m_wb_m2s,
         m_wb_s2m => m_wb_s2m
    );
end block;

--  s_axis_cc_m2s(1) <= c_axis_cc_m2s_zero;
  s_axis_cc_m2s(2) <= c_axis_cc_m2s_zero;
--  m_axis_cq_s2m(1) <= ( tready => '0');
  m_axis_cq_s2m(2) <= ( tready => '0');
  user_reset_n <= not user_reset;

   sys_reset_n_ibuf : IBUF
   port map (
      O => sys_rst_n_c, -- 1-bit output: Buffer output
      I => sys_rst_n  -- 1-bit input: Buffer input
   );
   
      
  refclk_ibuf : IBUFDS_GTE3
   generic map (
      REFCLK_HROW_CK_SEL => "00" -- Refer to Transceiver User Guide.
   )
   port map (
      O => sys_clk_gt,         -- 1-bit output: Refer to Transceiver User Guide.
      ODIV2 => sys_clk, -- 1-bit output: Refer to Transceiver User Guide.
      CEB => '0',     -- 1-bit input: Refer to Transceiver User Guide.
      I => sys_clk_p,         -- 1-bit input: Refer to Transceiver User Guide.
      IB => sys_clk_n        -- 1-bit input: Refer to Transceiver User Guide.
   );

end Behavioral;
